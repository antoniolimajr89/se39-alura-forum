package br.com.alura.forum.service;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alura.forum.dao.UsuarioDao;
import br.com.alura.forum.model.Usuario;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private Md5Service md5;
	
	public void logarUsuario(String email, String senha, HttpSession session) {
		Usuario encontrado = usuarioDao.buscarPorEmailESenha(email, md5.hashPassword(senha));
		if (encontrado == null) {
			throw new IllegalArgumentException("Dados invalidos!");
		}
		
		session.setAttribute("usuarioLogado", encontrado);
	}
	
}
