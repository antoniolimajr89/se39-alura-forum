package br.com.alura.forum.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.alura.forum.service.UsuarioService;

@Controller
public class LoginController {
	
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/login")
	public String carregarFormularioLogin() {
		return "form-login";
	}
	
	@PostMapping("/login")
	public String efetuarLogin(String email, String senha, HttpSession session) {
		try {
			usuarioService.logarUsuario(email, senha, session);
			return "redirect:/topicos";
		} catch (IllegalArgumentException e) {
			return "redirect:/login?error";
		}
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}

}
