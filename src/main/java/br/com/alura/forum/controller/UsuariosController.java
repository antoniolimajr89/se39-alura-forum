package br.com.alura.forum.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.alura.forum.dao.UsuarioDao;
import br.com.alura.forum.model.Usuario;
import br.com.alura.forum.service.CadastroUsuarioService;
import br.com.alura.forum.service.Md5Service;

@Controller
@RequestMapping("/usuarios")
public class UsuariosController {
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private CadastroUsuarioService cadastroUsuarioService;
	
	@Autowired
	private Md5Service md5;
	
	@GetMapping("/perfil")
	public String carregarPerfil(Model model, HttpSession session) {
		model.addAttribute("usuario", getUsuarioLogado(session));
		return "perfil";
	}
	
	@PostMapping("/perfil")
	@Transactional
	public String alterarDadosDoPerfil(Usuario editado, HttpSession session, RedirectAttributes redirectAttributes) {
		usuarioDao.atualizarDados(editado);
		atualizarDadosDoUsuarioLogado(session, editado);
		redirectAttributes.addFlashAttribute("mensagemSucesso", "Dados atualizados com sucesso!");
		return "redirect:/usuarios/perfil";
	}
	
	@PostMapping("/perfil/senha")
	@Transactional
	public String alterarSenha(String senhaAtual, String novaSenha, String confirmacaoNovaSenha, HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		Usuario logado = getUsuarioLogado(session);
		String senhaAtualMd5 = md5.hashPassword(senhaAtual);
		if (!senhaAtualMd5.equals(logado.getSenha())) {
			model.addAttribute("mensagemErro", "Senha atual não confere!");
			return carregarPerfil(model, session);
		}

		if (!novaSenha.equals(confirmacaoNovaSenha)) {
			model.addAttribute("mensagemErro", "Nova senha e confirmação não conferem!");
			return carregarPerfil(model, session);
		}
		
		logado.setSenha(md5.hashPassword(novaSenha));
		usuarioDao.atualizarDados(logado);
		atualizarDadosDoUsuarioLogado(session, logado);
		redirectAttributes.addFlashAttribute("mensagemSucesso", "Dados atualizados com sucesso!");
		return "redirect:/usuarios/perfil";
	}
	
	@GetMapping("/cadastro")
	public String carregarFormularioCadastro() {
		return "form-cadastro";
	}
	
	@PostMapping("/cadastro")
	public String cadastrarNovoUsuario(Usuario novo, String confirmacaoSenha, HttpSession session, Model model) {
		try {
			if (!novo.getSenha().equals(confirmacaoSenha)) {
				throw new IllegalArgumentException("Senha e confirmação senha não batem!");
			}
			cadastroUsuarioService.cadastrarNovoUsuario(novo);
			atualizarDadosDoUsuarioLogado(session, novo);
			return "redirect:/topicos";
		} catch (IllegalArgumentException e) {
			model.addAttribute("mensagemErro", e.getMessage());
			return "form-cadastro";
		}
	}
	
	private Usuario getUsuarioLogado(HttpSession session) {
		return (Usuario) session.getAttribute("usuarioLogado");
	}
	
	private void atualizarDadosDoUsuarioLogado(HttpSession session, Usuario usuario) {
		session.setAttribute("usuarioLogado", usuario);
	}
	
}
